var gulp = require('gulp');
var sass = require('gulp-sass');
var minifyCss = require('gulp-clean-css');
var watch = require('gulp-watch');
var concat = require('gulp-concat');
var merge = require('merge-stream');



gulp.task('minify-css', function() {
    var sassStream,
        cssStream;

    sassStream = gulp.src('src/styles/*.scss')
        .pipe(sass())
        .pipe(minifyCss())
        .pipe(concat('sass.css'));

    cssStream = gulp.src('src/styles/*.css')
        .pipe(minifyCss())
        .pipe(concat('css.css'));

    return merge(sassStream, cssStream)
        .pipe(concat('style.css'))
        .pipe(gulp.dest('dist/styles'));
});

gulp.task('default', function() {
    gulp.watch('src/styles/*.scss', ['minify-css']); });